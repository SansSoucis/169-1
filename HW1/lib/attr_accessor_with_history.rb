class Class
  def attr_accessor_with_history(attr_name)
    attr_name = attr_name.to_s # make sure it's a string
    attr_reader attr_name # create the attribute's getter
    attr_reader attr_name+"_history" # create bar_history getter

    #Here is some meta-programming : the Class gets 
    # one extra method related to that attribute :
    # "attr_name_history", wich will return an array containing the history.
    # Current value is not in the returned array.
    
    class_eval %Q{

      def #{attr_name}_history
        @#{attr_name}_history = nil unless defined? @#{attr_name}_history
        @#{attr_name}_history
      end

      def #{attr_name}=(value)
        @#{attr_name}_history = nil unless defined? @#{attr_name}_history
        if defined?@#{attr_name}
          @#{attr_name}_history = [nil]  if @#{attr_name}_history == nil
          @#{attr_name}_history << @#{attr_name}
        end
        @#{attr_name}
      end
    }
  end
end