#encoding : utf-8


module FunWithStrings

  def palindrome?
    begin
      funstring  = ""
      self.to_s.scan(/\w/) {|character| funstring += character }
      (funstring.downcase.reverse == funstring.downcase)
    rescue
      puts "#{__method__} :\n\t#{$!}"
      false
    end
  end

# encoding : utf-8

  def count_words
    begin
      funstring = self.to_s.downcase
      h = Hash.new
      funstring.scan(/\w+/).each do |word|
        if h.has_key?(word) 
          h[word] +=  1
        else 
          h[word] = 1
        end
      end
      h
    rescue 
      puts "#{__method__} :\n\t#{$!}"
      Hash.new
    end
  end



=begin
  
An anagram group is a group of words such that any one can be converted
into any other just by rearranging the letters. 
For example, "rats", "tars" and "star" are an anagram group.
Given a space separated list of words in a single string, 
write a method that groups them into anagram groups and 
returns the array of groups. 
Case doesn't matter in classifying string as anagrams 
(but case should be preserved in the output), 
and the order of the anagrams in the groups doesn't matter.
  
=end

def anagram_groups
    self.split(/ /).inject(Hash.new([])) { |myhash, word| 
            myhash[word.downcase.chars.sort.join]+=[word] ; myhash }.values
end

end # ! of module !

# make all the above functions available as instance methods on Strings:
class String
  include FunWithStrings
end