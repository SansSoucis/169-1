class RockPaperScissors

  # Exceptions this class can raise:
  class NoSuchStrategyError < StandardError ; end

  def self.winner(player1, player2)
    if !(["R","P","S"].include?(player1[1].upcase)) or !(["R","P","S"].include?(player2[1].upcase))
    	e =   NoSuchStrategyError.new "Strategy must be one of R,P,S"
  		raise e
    end
  	
  	a = [player1,player2]
  	strat_p1 = a[0][1].downcase
  	strat_p2 = a[1][1].downcase 
  	strat_p1p2 = strat_p1 + strat_p2
    winner_is = case 
      when strat_p1p2 == "rs" then a[0]
      when strat_p1p2 == "sp" then a[0]
      when strat_p1p2 == "pr" then a[0]
      when strat_p1p2 == "sr" then a[1]
      when strat_p1p2 == "ps" then a[1]
      when strat_p1p2 == "rp" then a[1]
      when strat_p1 == strat_p2 then a[0]
    end
    return winner_is
  end

  def self.tournament_winner(tournament)
    if tournament[0].flatten(1).size != tournament[0].size
      player1 = self.tournament_winner(tournament[0])
    else
      player1 = tournament[0]
    end
    if tournament[1].flatten(1).size != tournament[1].size
      player2 = tournament_winner(tournament[1])
    else
      player2 = tournament[1]
    end

    self.winner(player1,player2)

  end

end
