module MoviesHelper
  # Checks if a number is odd:
  def oddness(count)
    count.odd? ?  "odd" :  "even"
  end

  def hilight title
    'hilite' if session[:movies_index_sort_by] ==  title
  end

end
