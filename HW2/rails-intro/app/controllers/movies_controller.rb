#encoding : utf-8
class MoviesController < ApplicationController
  attr_accessor :filters, :all_ratings, :order_by, :hilite

  def initialize
    super
    # don't need to require that every now and then, at least for now.
    @all_ratings = Movie.get_ratings
  end


  def show
    id = params[:id] # retrieve movie ID from URI route
    @movie = Movie.find(id) # look up movie by unique ID
    # will render app/views/movies/show.<extension> by default
  end



  def push_params_to_session
    all_ratings_hash = {}
    @all_ratings.each {|rating| all_ratings_hash[rating]=0}
    session[:movies_index_ratings] = params[:ratings] unless (params[:ratings].nil?)
    session[:movies_index_ratings] = all_ratings_hash if session[:movies_index_ratings].nil?
    session[:movies_index_sort_by] = params[:sort_by] unless params[:sort_by].nil?
    session[:movies_index_sort_by] = 'id' if session[:movies_index_sort_by].nil?        
    params[:ratings] = session[:movies_index_ratings]
    params[:sort_by] = session[:movies_index_sort_by]
    params[:utf8]="✓"
  end


  def set_filters params
    answer = ['movies.rating in (?)', [] ]
    session[:movies_index_ratings].each { |rating| answer[1] << rating[0] }
    @hilite = session[:movies_index_sort_by]
    @order_by = "#{@hilite} asc"
    answer
  end



  # given the current filter status, creates an array with the checked status of each 
  # rating in the index view form.
  def build_checked_array current_filters, all_ratings
    @checked_array = []
    all_ratings.each do |rating|
      @checked_array << (current_filters[1].include?(rating) ?  true : false )
    end
    @checked_array
  end

  def index


    redirect_flag = ((session[:movies_index_ratings] == params[:ratings]) &&
     (session[:movies_index_sort_by] == params[:sort_by]) && 
     (!session[:movies_index_ratings].nil?) &&
     (!session[:movies_index_sort_by].nil?))


    push_params_to_session
    unless redirect_flag 
      return redirect_to movies_path(params) #and return
    end
    @filters = set_filters params 
    puts "@@@@@@@@@@@@@@@@@@"
    puts "filters : #{@filters}"
    puts "@@@@@@@@@@@@@@@@@@"
    build_checked_array @filters, @all_ratings
    @movies = Movie.where(@filters).order(@order_by).all
  end


  def new
    # default: render 'new' template
  end

  def create
    @movie = Movie.create!(params[:movie])
    flash[:notice] = "#{@movie.title} was successfully created."
    redirect_to movies_path
  end

  def edit
    @movie = Movie.find params[:id]
  end

  def update
    @movie = Movie.find params[:id]
    @movie.update_attributes!(params[:movie])
    flash[:notice] = "#{@movie.title} was successfully updated."
    redirect_to movie_path(@movie)
  end

  def destroy
    @movie = Movie.find(params[:id])
    @movie.destroy
    flash[:notice] = "Movie '#{@movie.title}' deleted."
    redirect_to movies_path
  end

end
