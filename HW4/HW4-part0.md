
#Clone project
    git clone https://github.com/saasbook/bdd-tdd-cycle

# Configure / migrate
    bundle install --without production
    bundle exec rake db:migrate
    -- After a migration, one must think of doing a 
    rake db:test:prepare

# Create the cucumber / rspec squeletton
    rails generate cucumber:install capybara
    rails generate cucumber_rails_training_wheels:install
    rails generate rspec:install

#test
    bundle exec rake spec
    bundle exec rake cucumber

# Create a bitbucket depo
git remote set-url origin https://e-jambon@bitbucket.org/e-jambon/hw4.git
git push -u origin --all
git push -u origin --tags