require 'debugger'
require 'spec_helper'

describe MoviesController do


  describe "Adding a director" do
    before :each do
      @Starwars = double(Movie, title: "Star Wars", director: "Lucas", id: 1)
      Movie.stub(:find).with("1").and_return(@Starwars)
    end
    it 'should redirect' do
      @Starwars.stub(:update_attributes).and_return(true)
      @Starwars.stub(:valid?).and_return(true)
      @Starwars.stub(:save).and_return(@Starwars)
      put :update, {id: '1' , movie: @Starwars}
      response.should redirect_to(movie_path(@Starwars))
    end
    it 'should redirect to edit if not valid (sad path)' do
      @Starwars.stub(:update_attributes).and_return(true)
      @Starwars.stub(:valid?).and_return(false)
      stub(:flash)
      put :update, {id: '1' , movie: @Starwars}
      response.should redirect_to edit_movie_path(@Starwars)
    end
  end




end

