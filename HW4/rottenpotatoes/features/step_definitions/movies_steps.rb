

Given /the following movies exist/ do |movies_table|
  movies_table.hashes.each do |movie|
  	Movie.create( movie )
  end
end


Then /the director of "(.*)" should be "(.*)"/ do |movie, director|
	a = Movie.find_by_title(movie).director
	assert a == director, "The director #{a} was not saved properly"
end

# And /I should see "'(.*)' has no director info"/ do |movie_title|
# 	debugger
# 	if page.body.find ("#{movie_title})' has no director info")
# 		puts "FOUND"
# 	else 
# 		puts "NOT FOUND"
# 	end
# end 