class Movie < ActiveRecord::Base

  attr_accessible :title, :rating, :description, :release_date, :director
  validates_uniqueness_of :title, :scope => :release_date, message: " Already in the database with the same release date"


  def self.all_ratings
    %w(G PG PG-13 NC-17 R)
  end

  def same_director_movies selected_ratings, ordering
  	movies =  Movie.find_all_by_rating(selected_ratings, ordering)
  	movies.select! {|elem| elem.director == director }  unless ( (director =='')||(director.nil?) )
	return movies
  end

end


