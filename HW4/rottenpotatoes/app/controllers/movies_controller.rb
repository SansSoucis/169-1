class MoviesController < ApplicationController

  def show
    id = params[:id] # retrieve movie ID from URI route
    @movie = Movie.find(id) # look up movie by unique ID
    # will render app/views/movies/show.<extension> by default
  end

  def set_ordering
    @sort = params[:sort] || session[:sort]
    case @sort
    when 'title'
      @ordering, @title_header = {:order => :title}, 'hilite'
    when 'release_date'
      @ordering, @date_header = {:order => :release_date}, 'hilite'
    end
  end

  def set_selected_ratings
    @selected_ratings = params[:ratings] || session[:ratings] || {}
    if @selected_ratings == {}
      @selected_ratings = Hash[@all_ratings.map {|rating| [rating, rating]}]
    end
  end
 

  def synchronized_index_params_session?
    if params[:sort] != session[:sort]
      session[:sort] = @sort
      return false
    end

    if params[:ratings] != session[:ratings] and @selected_ratings != {}
      session[:sort] = @sort
      session[:ratings] = @selected_ratings
      return false
    end
    return true
  end 

  def analyzed_order_and_ratings?
    @all_ratings = Movie.all_ratings
    set_ordering
    set_selected_ratings
    unless synchronized_index_params_session?
      flash.keep
      redirect_to :sort => @sort, :ratings => @selected_ratings 
      return false
    end
    true
  end
    

  def index
    return unless analyzed_order_and_ratings?
    @movies = Movie.find_all_by_rating(@selected_ratings.keys, @ordering)  
    render :index
  end

  def new
    # default: render 'new' template
  end

  def create
    @movie = Movie.create!(params[:movie])
    flash[:notice] = "#{@movie.title} was successfully created."
    redirect_to movies_path
  end

  def edit
    @movie = Movie.find params[:id]
  end

  def update
    @movie = Movie.find params[:id]
    @movie.update_attributes(params[:movie])
    if !@movie.valid?
      flash[:notice] = "Not valid"
      redirect_to edit_movie_path @movie
      return    
    end
    @movie.save
    #@movie.update_attributes!(params[:movie])
    flash[:notice] = "#{@movie.title} was successfully updated."
    redirect_to movie_path @movie
  end

  def destroy
    @movie = Movie.find(params[:id])
    @movie.destroy
    flash[:notice] = "Movie '#{@movie.title}' deleted."
    redirect_to movies_path
  end

  def director
    @movie = Movie.find params[:id]
    return unless analyzed_order_and_ratings?
    @movies = @movie.same_director_movies(@selected_ratings.keys, @ordering)
    if ((@movie.director == '') || @movie.director.nil? )
      flash[:notice] =  "'#{@movie.title}' has no director info"
      redirect_to '/movies' and return
    end
  end

end
