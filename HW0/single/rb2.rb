#!/usr/bin/ruby
# encoding : utf-8


require 'date'


puts "========================"
puts " Getting info"
puts "========================"
puts "Quel est votre nom ?" 
nom = gets
nom = nom.chomp
puts "Bonjour #{nom}."

puts "========================"
puts " Get 2 words, print them in reverse order" 
puts "========================"
puts "Ecrivez exactement deux mots"
words = gets.chomp
words = words.split " "
puts "#{words[0].reverse} #{words[1].reverse}"

puts "========================"
puts "Nom, age => date de naissance" 
puts "========================"
puts "Quel est votre nom ?"
nom = gets.chomp
puts "Quel est votre age ?"
age = gets.chomp.to_i

ajourdhui = Date.today

puts "Bonjour, #{nom}, vous êtes né en " \
    + "#{ajourdhui.prev_year(age).year}"


