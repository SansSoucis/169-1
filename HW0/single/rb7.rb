#!/usr/bin/ruby
# encoding : utf-8

log = [{:time=>201201, :x=>2}, {:time=>201201, :y=>7}, {:time=>201201, :z=>2}, {:time=>201202, :a=>3}, {:time=>201202, :b=>4}, {:time=>201202, :c=>0}]
# foo(log) must return
# => [{:time=>201201, :x=>2, :y=>7, :z=>2}, {:time=>201202, :a=>3, :b=>4, :c=>0}]

def foo (log)
	merging = lambda {|a_hash,aha_hash| a_hash.merge! aha_hash }
	result = []

	log.each do  |item| 
		result << item unless result.count > 0
		if result.last[:time] == item[:time]
			result[result.count-1] = merging.call result.last, item
		
		else
			result << item
		end
	end
	result
end

 
puts "#{foo(log)}"
