#!/home/ygueron/.rvm/rubies/ruby-1.9.3-p545/bin/ruby
# encoding : utf-8

# Premiers pas
x = 2 + 4 + 6 + 8
x /= 5
y = 2 * 3 * 4
y -= x

puts "Premier calcul : #{y.to_s}"

# Des variables.
nom = "Yann"
age = 44
puts "#{nom}, #{age.to_s} ans apprend à coder en Ruby"

#apprentissage des boucles.
puts "========================"
puts "Traverseur numérique de 1 à 10" 
puts "========================"

traverse_numerique = 0
10.times do
 traverse_numerique += 1
 puts "#{traverse_numerique.to_s}"
end

# somme des nombres de 1 à 11
puts "========================"
puts "= somme des nombre de 1 à n, pour les 11 premiers entiers ="
puts "========================"
total = somme = 0
11.times do 
 total +=1
 somme += total
 puts "somme pour #{total.to_s} :  #{somme.to_s}"
end

#Une instruction sur plusieurs lignes
puts "========================="
puts " Ceci est une ligne de texte " + \
       "rédigée sur plusieurs lignes"
puts "========================="

#factorielle
puts "========================="
puts "Petite factorielle entre amis"
puts " pour la valeur 6"
puts "========================="
nombre = 6
total = 0
produit = 1

nombre.times do 
 total +=1
 produit *= total
end
puts nombre.to_s + "! = " \
       + produit.to_s

puts "========================="
puts "1 km à pied...100"
puts "========================="
parcouru = 0
100.times do 
 parcouru += 1
 puts "#{parcouru.to_s} kilomètre à pied, ça use, ça use"
 puts "#{parcouru.to_s} kilomètre à pied, ça use les souliers"
end

