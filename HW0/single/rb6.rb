#!/usr/bin/env ruby 

def mylambda 
	lambda {|a,b| a + b}
end

def callthelambda a,b 
	yield(a,b)
end

 
puts "#{mylambda.call(3,5)}"
doh = mylambda
puts "#{ callthelambda(3,5) {|a,b| doh.call(a,b)}}"
