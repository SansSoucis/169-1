#!/usr/bin/ruby
# encoding : utf-8

puts "Calcul factoriel. Saisissez le nombre :"
nombre = gets.chomp.to_i
iter = 0
fact = 1
nombre.times do
 iter += 1
 fact *= iter
end

puts "#{nombre}! = #{fact.to_s}"
