=begin
Define a class BookInStock which represents a book with an isbn number, isbn, and price of the book as a 
floating-point number, price, as attributes. The constructor should accept the ISBN number (a string) as the 
first argument and price as second argument, and should raise ArgumentError (one of Ruby's built-in 
exception types) if the ISBN number is the empty string or if the price is less than or equal to zero.
Include the proper getters and setters for these attributes. Include a method price_as_string that returns 
the price of the book with a leading dollar sign and trailing zeros, that is, a price of 20 should display as 
"$20.00" and a price of 33.8 should display as "$33.80".
=end



class BookInStock



	#accept the ISBN number (a string) as the first argument and price as second argument
	def initialize (isbn, price)
		self.isbn= isbn 
		self.price= price.to_f
	end
    
   
    # isbn writer accessor :  raises an ArgumentError if isbn_number is not a string or is empty.
	def isbn=(isbn_number)
		raise ArgumentError.new("BookInStock isbn must not a nonempty string.") unless ( (isbn_number.is_a?(String)) && (isbn_number !="") ) 
		@isbn = isbn_number
	end
    
    # isbn reader accessor : returns the isbn number of the BookInStock
    def isbn
        @isbn
    end
	
    #price writer accessor :  raises an ArgmentError if price is not a positive float.
	def price=(price)
		raise ArgumentError.new("Price must be a positive float") unless (price.is_a?(Float)  && (price > 0)) 
		@price = price
	end

	# price reader accessor : returns the price of the BookInStock
    def price
        @price
    end

    def price_as_string
    	sprintf('$%0.2f', @price)
  	end
end



