#!/usr/bin/ruby
# encoding : utf-8
 
# info :
# pairing with <nitesco> 

# checks if arg1 is an array of integers
# => true if it is, false if not.
def is_array_of_integer?(anyArray)
    if anyArray.is_a?(Array)
        return anyArray.all? { |item| item.is_a?(Fixnum)} 
    else
        return false
    end
end



# Define a method sum which takes an array of integers as an argument 
# and returns the sum of its elements. For an empty array 
# it should return zero.

def sum(array)
  raise "#{__method__} : argument must be array" unless array.is_a?(Array)
  return 0 if array.empty? 
  raise "#{__method__} : argument must be array of Integers" unless is_array_of_integer?(array)
  return array.reduce(:+)
end



#Define a method max_2_sum which takes an array of integers as an argument and 
#returns the sum of its two largest elements. For an empty array it should 
#return zero. For an array with just one element, it should return that element.

def max_2_sum(array)
 raise "#{__method__} : argument must be array" unless array.is_a?(Array)
 return 0 if array.empty? 
 raise "#{__method__} : argument must be array of Integers" unless is_array_of_integer?(array)
 return array[0] if array.length == 1
 return sum array.sort.last(2)
end






#Define a method sum_to_n? which takes an array of integers and an 
#additional integer, n, as arguments and returns true if any two distinct 
#elements in the array of integers sum to n. An empty array or single 
#element array should both return false.



def sum_to_n?(array, n)
    raise "#{__method__} : argument must be array" unless array.is_a?(Array)
    return false if (array.empty? || array.length == 1)
    
    mapped_hash = Hash.new
    array.each_with_index {|value,index| mapped_hash[index]=value } 

    array.each_with_index do |value,index| 
        temp_hash = mapped_hash.select{|key, val| key != index } #must be a distinct item
        if temp_hash.has_value?(n-value)  
         return true 
        end
    end
    false
end




