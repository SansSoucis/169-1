#!/usr/bin/ruby
# encoding : utf-8


# hello (name)
# takes a string representing a name and returns the string "Hello, " concatenated with the name.
def hello(name)
  raise "#{__method__} : argument must be a String.\n #{name} is #{name.class}." if !name.is_a?(String)
  return "Hello, #{name}"
end



# starts_with_consonant?(s)
# takes a string and returns true if it starts with a consonant and false otherwise. 
# (For our purposes, a consonant is any letter other than A, E, I, O, U.) 
# NOTE: be sure it works for both upper and lower case and for nonletters! 
def starts_with_consonant?(s)
  raise "#{__method__} : argument must be a String.\n #{s} is #{s.class}." unless s.is_a?(String)
  !((s =~ /\A[A-Z&&[^AEIOU]]/i) == nil) 
end


# binary_multpile_of_4?(s) 
# that takes a string and returns true 
#if the string represents a binary number that is a multiple of 4. 
#NOTE: be sure it returns false if the string is not a valid binary number! 
def binary_multiple_of_4?(s)
   return true if s == "0"	
   s =~ /^[01]+00$/ 
end