require_relative '../lib/HW0part3.rb'

describe BookInStock, "#initialize:" do
    it "fails if the ISBN is an empty string" do
        expect { book = BookInStock.new('', 3) }.to raise_error
    end

    it "fails if the price is = 0" do
        expect { book = BookInStock.new('arst', 0) }.to raise_error
    end

    it "fails if the price is < 0" do
        expect { book = BookInStock.new('arst', -5) }.to raise_error
    end

    it "works if the ISBN is non-empty and the price is positive" do
        book = BookInStock.new('ARST', 10)
        book.isbn.should eq('ARST')
        book.price.should eq(10)
    end
end

describe BookInStock, "#price_as_string:" do
    it "returns $10.00 if the price is 10" do
        book = BookInStock.new('ARST', 10)
        book.price_as_string.should eq('$10.00')
    end

    it "returns $10.40 if the price is 10.4" do
        book = BookInStock.new('ARST', 10.4)
        book.price_as_string.should eq('$10.40')
    end

    it "returns $10.45 if the price is 10.45" do
        book = BookInStock.new('ARST', 10.45)
        book.price_as_string.should eq('$10.45')
    end
end

describe BookInStock, "getters and setters:" do
    it "defines a getter for the ISBN" do
        book = BookInStock.new('ARST', 20.45)
        book.respond_to?(:isbn).should be_true
    end

    it "defines a setter for the ISBN" do
        book = BookInStock.new('ARST', 20.45)
        book.respond_to?(:isbn=).should be_true
    end

    it "defines a getter for the price" do
        book = BookInStock.new('ARST', 20.45)
        book.respond_to?(:price).should be_true
    end

    it "defines a setter for the price" do
        book = BookInStock.new('ARST', 20.45)
        book.respond_to?(:price=).should be_true
    end
end