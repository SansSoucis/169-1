
require_relative '../lib/HW0part1.rb'


describe "Testing sum():" do
    it "returns the sum of all the elements" do
        array = [1, 3, 6, 1, 2]
        sum(array).should eq(13)
    end

    it "returns 0 for an empty array" do
        array = []
        sum(array).should eq(0)
    end
end

describe "Testing max_2_sum():" do
    it "returns the sum of the two highest elements" do
        array = [1, 3, 6, 1, 2]
        max_2_sum(array).should eq(9)
    end

    it "returns the sum of the two highest elements when they are equal" do
        array = [1, 6, 6, 1, 2]
        max_2_sum(array).should eq(12)
    end

    it "returns 0 for an empty array" do
        array = []
        max_2_sum(array).should eq(0)
    end

    it "returns the first element if the array contains one element" do
        array = [42]
        max_2_sum(array).should eq(42)
    end
end

describe "Testing sum_to_n?():" do
    it "returns true for ([1, 3, 6, 1, 2], 4)" do
        array = [1, 3, 6, 1, 2]
        sum_to_n?(array, 4).should eq(true)
    end

    it "returns false for ([12, 35, 64, 12, 25], 11)" do
        array = [12, 35, 64, 12, 25]
        sum_to_n?(array, 11).should eq(false)
    end

    it "returns false when the sum is greater that the 2 largest elements" do
        array = [12, 35, 64, 12, 25]
        nb = max_2_sum(array) + 1
        sum_to_n?(array, nb).should eq(false)
    end
    
    it "returns true for ([1,2,5,5], 10)" do
        array = [1,2,5,5]
        sum_to_n?(array, 10).should eq(true)
    end
end