require_relative '../lib/HW0part2.rb'



describe "Testing hello():" do
    it "returns 'Hello, <param>'" do
        name = 'Sam I Am'
        hello(name).should eq('Hello, Sam I Am')
    end
end


describe "Testing starts_with_consonant?():" do
    it "returns true when it starts with a consonant" do
        starts_with_consonant?('consonent').should be_true
    end

    it "returns true when it starts with an uppercase consonant" do
        starts_with_consonant?('CONSONANT').should be_true
    end

    it "returns false when it starts with a vowel" do
        starts_with_consonant?('ahaha').should be_false
    end

    it "returns false when it starts with an uppercase vowel" do
        starts_with_consonant?('AHAHA').should be_false
    end

    it "returns false when it starts with another character" do
        starts_with_consonant?('#aaaa').should be_false
    end
end

describe "Testing binary_multiple_of_4?():" do
    it "returns true for a binary multiple of 4" do
        binary_multiple_of_4?('011101010100').should be_true
    end

    it "returns true for '0'" do
        binary_multiple_of_4?('0').should be_true
    end

    it "returns false for a binary not multiple of 4" do
        binary_multiple_of_4?('011101010101').should be_false
    end

    it "returns false for a non-binary string" do
        binary_multiple_of_4?('011103010100').should be_false
    end
end