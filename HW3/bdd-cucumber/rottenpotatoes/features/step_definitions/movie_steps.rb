# Add a declarative step here for populating the DB with movies.
  

Given /the following movies exist/ do |movies_table|
  movies_table.hashes.each do |movie|
    # each returned element will be a hash whose key is the table header.
    # you should arrange to add that movie to the database here.
   a = Movie.new( title: movie[:title], rating: movie[:rating], release_date: movie[:release_date])
   a.save 
  end

  #flunk "Unimplemented"
end

# Make sure that one string (regexp) occurs before or after another one
#   on the same page

Then /I should see "(.*)" before "(.*)"/ do |e1, e2|
  #  ensure that that e1 occurs before e2.
  #  page.body is the entire content of the page as a string.
  


  pos_e1 = page.body.index(e1)
  pos_e2 = page.body.index(e2)
  assert_operator pos_e1, :< , pos_e2 ,  "Fail --->#{e1} was at position : #{pos_e1}.  And #{e2} was found at position #{pos_e2}"
  puts  "Pass ---> #{e1} was found before #{e2}"


end


# Make it easier to express checking or unchecking several boxes at once
#  "When I uncheck the following ratings: PG, G, R"
#  "When I check the following ratings: G"

When /I (un)?check the following ratings : (.*)/ do |uncheck, rating_list|
  # HINT: use String#split to split up the rating_list, then
  #   iterate over the ratings and reuse the "When I check..." or
  #   "When I uncheck..." steps in lines 89-95 of web_steps.rb
  rating_list.split(',').each do |field| 
    uncheck ?  uncheck("ratings_#{field}") : check("ratings_#{field}")
  end
end

When /I click the button : (.*)/ do |button|
  click_button(button)
end


Then /I should see the following ratings : (.*)/ do |rating_list|

  rating_list.split(',').each do |rating|
    page.html.should include("<td>#{rating}</td>")
  end
  #flunk "Unimplemented"
end

Then /I should not see the following ratings : (.*)/ do |rating_list|

  rating_list.split(',').each do |rating|
    page.html.should_not include("<td>#{rating}</td>")
  end
  #flunk "Unimplemented"
end



Then /I should see all the movies/ do
  # Make sure that all the movies in the app are visible in the table
  movies = Movie.all
  movies.each do |movie|
    page.html.should include("<td>#{movie.title}</td>")
    page.html.should include("<td>#{movie.rating}</td>")
    page.html.should include("<td>#{movie.release_date}</td>")
  end
  #flunk "Unimplemented"
end
